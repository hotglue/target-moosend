"""moosend target sink class, which handles writing streams."""

from singer_sdk.sinks import BatchSink
import requests


class moosendSink(BatchSink):
    """moosend target sink class."""

    max_size = 1000

    headers = {
        "Content-Type": "application/json",
        "Accept": "application/json"
    }
    list_id = None

    @property
    def url_base(self):
        return f"https://api.moosend.com/v3/subscribers/{self.list_id}/"

    @property
    def params(self):
        return dict(apikey=self.config.get("api_key"))

    def _request(self, endpoint, payload):
        url = f"{self.url_base}{endpoint}"
        response = requests.post(url, json=payload, params=self.params, headers=self.headers)
        response.raise_for_status()
        response = response.json()
        if response.get("Error"):
            self.logger.warning(response.get("Error"))
        return response

    def start_batch(self, context: dict) -> None:
        """Start a batch."""
        context["Lists"] = {}
    

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record. """
        list_id = None
        if "list_id" in record:
            list_id = record["list_id"]
            #If list id in record is null use default list id
            if list_id is None:
                list_id = self.config.get("list")
        else:
            list_id = self.config.get("list")        

        #There was no list id in record or the config.
        if list_id is None:
            raise Exception("ListId is required.")    

        if not list_id in context['Lists']:
            context['Lists'][list_id] = {}
            context['Lists'][list_id]["Contacts"] = {}
            context['Lists'][list_id]["Contacts"]["HasExternalDoubleOptIn"] = self.config.get("external_opt_in")
            context['Lists'][list_id]["Contacts"]["Subscribers"] = []
            context['Lists'][list_id]["ContactsUpdate"] = []

        if "Id" in record:
            subscriber_id = record.pop("Id")
            context['Lists'][list_id]["ContactsUpdate"].append((subscriber_id, record))
        else:
            context['Lists'][list_id]["Contacts"]["Subscribers"].append(record)

    def process_batch(self, context: dict) -> None:
        """Write out any prepped records and return once fully written."""
        for moosend_list in context['Lists'].keys():
            self.list_id = moosend_list
            self._request("subscribe_many.json", context['Lists'][moosend_list]["Contacts"])
            for subs in context['Lists'][moosend_list]["ContactsUpdate"]:
                self._request(f"update/{subs[0]}.json", subs[1])
