"""moosend target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_moosend.sinks import (
    moosendSink,
)


class TargetMoosend(Target):
    """Sample target for moosend."""

    name = "target-moosend"
    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
            description="The api_key of moosend API"
        ),
        th.Property(
            "list",
            th.StringType,
            required=False,
            description="The target contact list."
        ),
        th.Property(
            "external_opt_in",
            th.BooleanType,
            default=False,
            description="The target contact list."
        )

    ).to_dict()
    default_sink_class = moosendSink

if __name__=="__main__":
    TargetMoosend.cli()